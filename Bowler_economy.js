const convertCvsIntoArray = require("./CVSIntoArray");

function getBowlerEconomy(error, data) {
    if (error) {
        console.error(error)
    } else {
        let database = data.split('\n')
        let idRange = {}
        convertCvsIntoArray('./archive/matches.csv', function (err, data1) {
            if (err) {
                console.error(err)
            } else {
                let database1 = data1.split('\n')
                for (let index = 0; index < database1.length; index++) {
                    if (database1[index].split(',')[1] === '2015') {
                        idRange[database1[index].split(',')[0]] = 1
                    }
                }
                // console.log(idRange)
                let bowlerStats = {}
                for (let index = 0; index < database.length; index++) {
                    let currentArray = database[index].split(',')
                    if (idRange[currentArray[0]]) {
                        if (bowlerStats[currentArray[8]]) {
                            bowlerStats[currentArray[8]] = {
                                deliveries: (bowlerStats[currentArray[8]].deliveries) + 1,
                                runs: bowlerStats[currentArray[8]].runs + parseFloat(currentArray[17])
                            }
                        } else {
                            bowlerStats[currentArray[8]] = {deliveries: 1, runs: parseFloat(currentArray[17])}

                        }
                    }
                }
                let economy = {}
                for (let bowler in bowlerStats) {
                    economy[bowler] = ((bowlerStats[bowler].runs) / (bowlerStats[bowler].deliveries)) * 6
                }
                console.log(economy)
            }
        })

    }
}

//

convertCvsIntoArray('./archive/deliveries.csv', getBowlerEconomy)