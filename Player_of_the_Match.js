const convertCvsIntoArray = require("./CVSIntoArray");

function playerOfMatch(error, data) {
    if (error) {
        console.log(error)
    } else {
        let manOfTheMatch = {}
        let database = data.split('\n')
        for (let index = 1; index < database.length - 1; index++) {
            let year = database[index].split(',')[1]
            let player = database[index].split(',')[13]
            if (manOfTheMatch[year]) {
                if (manOfTheMatch[year][player]) {
                    manOfTheMatch[year][player] = manOfTheMatch[year][player] + 1
                } else {
                    manOfTheMatch[year][player] = 1
                }
            } else {
                manOfTheMatch[year] = {}
                manOfTheMatch[year][player] = 1
            }
        }
        let playerOfTheSeason = {}
        for (let year in manOfTheMatch) {
            let times = 0
            let _player
            for (let player in manOfTheMatch[year]) {
                if (manOfTheMatch[year][player] > times) {
                    times = manOfTheMatch[year][player]
                    _player = player
                }
            }
            playerOfTheSeason[year] = _player
        }
        console.log(playerOfTheSeason)
    }
}

convertCvsIntoArray('./archive/matches.csv', playerOfMatch)