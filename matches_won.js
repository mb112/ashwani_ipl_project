const convertCvsIntoArray = require('./CVSIntoArray')


function matchesWon(error, data) {
    if (error) {
        console.error(error)
    } else {
        let database = data.split('\n')
        let winsByTeams = {}
        for (let index = 1; index < database.length - 1; index++) {
            if (database[index].split(',')[10] !== '') {
                if (winsByTeams[database[index].split(',')[10]]) {
                    winsByTeams[database[index].split(',')[10]] = winsByTeams[database[index].split(',')[10]] + 1
                } else {
                    winsByTeams[database[index].split(',')[10]] = 1
                }
            }
        }
        console.log(winsByTeams)
    }
}


convertCvsIntoArray('./archive/matches.csv', matchesWon)