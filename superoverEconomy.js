const convertCvsIntoArray = require("./CVSIntoArray");

function getSuperOverEconomy(error, data) {
    if (error) {
        console.error(error)
    } else {
        let database = data.split('\n')
        let bowlerStats = {}
        for (let index = 1; index < database.length - 1; index++) {
            let currentArray = database[index].split(',')
            if (currentArray[9] !== '0') {
                if (bowlerStats[currentArray[8]]) {
                    bowlerStats[currentArray[8]] = {
                        deliveries: (bowlerStats[currentArray[8]].deliveries) + 1,
                        runs: bowlerStats[currentArray[8]].runs + parseFloat(currentArray[17])
                    }
                } else {
                    bowlerStats[currentArray[8]] = {deliveries: 1, runs: parseFloat(currentArray[17])}

                }
            }
        }
        // console.log(bowlerStats)
        let economy = {}
        let temp = 0
        let _player
        for (let player in bowlerStats) {
            let _economy = (bowlerStats[player].runs) / (bowlerStats[player].deliveries)
            if (_economy > temp) {
                temp = _economy
                _player = player
            }
            if(player==='MG Johnson'){
                economy[_player] = temp
            }
        }
        console.log(economy)
    }
}

convertCvsIntoArray('./archive/deliveries.csv', getSuperOverEconomy)