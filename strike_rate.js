const convertCvsIntoArray = require("./CVSIntoArray");

function strikeRate(error, data) {
    if (error) {
        console.error(error)
    } else {
        let batsmanStats = {}
        let database = data.split('\n')
        let idRange = {}
        convertCvsIntoArray('./archive/matches.csv', function (error, data1) {
            if (error) {
                console.error(error)
            } else {
                let database1 = data1.split('\n')
                for (let i = 1; i < database1.length - 1; i++) {
                    let currentArray = database1[i].split(',')
                    idRange[currentArray[0]] = currentArray[1]
                }
                // console.log(idRange)
                for (let index = 1; index < database.length - 1; index++) {
                    let currentArray = database[index].split(',')
                    if (currentArray[6] === 'DA Warner') {
                        if (batsmanStats[idRange[currentArray[0]]]) {
                            batsmanStats[idRange[currentArray[0]]] = {
                                balls: batsmanStats[idRange[currentArray[0]]].balls + 1,
                                runs: batsmanStats[idRange[currentArray[0]]].runs + parseFloat(currentArray[17])
                            }
                        } else {
                            batsmanStats[idRange[currentArray[0]]] = {balls: 1, runs: parseFloat(currentArray[17])}
                        }
                    }
                }
            }
            for (let year in batsmanStats) {
                batsmanStats[year] = (batsmanStats[year].runs) / (batsmanStats[year].balls)
            }
            console.log(batsmanStats)
        })
    }
}

convertCvsIntoArray('./archive/deliveries.csv', strikeRate)

