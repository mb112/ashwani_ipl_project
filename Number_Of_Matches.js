const convertCvsIntoArray = require('./CVSIntoArray')


function numberOfMatches(error, data) {
    if (error) {
        console.error(error)
    } else {
        const database = data.split('\n')
        let matchesPerYear = {}
        for (let index = 1; index < database.length - 1; index++) {
            if (matchesPerYear[database[index].split(',')[1]]) {
                matchesPerYear[database[index].split(',')[1]] = matchesPerYear[database[index].split(',')[1]] + 1
            } else {
                matchesPerYear[database[index].split(',')[1]] = 1
            }
        }
        console.log(matchesPerYear);
    }
}

convertCvsIntoArray('./archive/matches.csv', numberOfMatches)


