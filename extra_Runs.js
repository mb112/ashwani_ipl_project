const convertCvsIntoArray = require('./CVSIntoArray')

function extraRuns(error, data) {
    if (error) {
        console.error(error)
    } else {
        let database = data.split('\n')
        let idRange = {}
        convertCvsIntoArray('./archive/matches.csv', function (error, data1) {
            if (error) {
                console.error(error)
            } else {
            }
            let database1 = data1.split('\n')
            for (let i = 0; i < database1.length; i++) {
                let currentArray = database1[i].split(',')
                if (currentArray[1] === '2016') {
                    idRange[currentArray[0]] = 1
                }
            }
            let teamExtraRuns = {}
            for(let index =0;index<database.length;index++){
                let currentArray = database[index].split(',')
                if(idRange[currentArray[0]]){
                    if(teamExtraRuns[currentArray[3]]){
                        teamExtraRuns[currentArray[3]] = teamExtraRuns[currentArray[3]] + parseFloat(currentArray[16])
                    }else {
                        teamExtraRuns[currentArray[3]] = parseFloat(currentArray[16])
                    }
                }
            }
            console.log(teamExtraRuns)
        })

    }
}

convertCvsIntoArray('./archive/deliveries.csv', extraRuns)
